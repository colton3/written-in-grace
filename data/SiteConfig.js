const config = {
  siteTitle: "Written in Grace", // Site title.
  siteTitleShort: "Written in Grace", // Short site title for homescreen (PWA). Preferably should be under 12 characters to prevent truncation.
  siteTitleAlt: "Written in Grace", // Alternative site title for SEO.
  siteLogo: "/content/images/grace-facebook.png", // Logo used for SEO and manifest.
  siteUrl: "https://writteningrace.com", // Domain of your website without pathPrefix.
  pathPrefix: "", // Prefixes all links. For cases when deployed to example.github.io/gatsby-advanced-starter/.
  siteDescription: "Real life problems discussed in a down to earth fashion.", // Website description used for RSS feeds/meta description tag.
  siteRss: "/rss.xml", // Path to the RSS file.
  siteFBAppID: "", // FB Application ID for using app insights
  googleAnalyticsID: "UA-161598193-1", // GA tracking ID.
  dateFromFormat: "YYYY-MM-DD", // Date format used in the frontmatter.
  dateFormat: "DD/MM/YYYY", // Date format for display.
  userName: "Katie Plumley", // Username to display in the author segment.
  userTwitter: "gatsbyjs", // Optionally renders "Follow Me" in the Bio segment.
  userAvatar: "/static/e936083b1463704540a3875b2d380195/cb3d3/avatar.jpg", // User avatar to display in the author segment.
  userDescription: "I am a Christian, mom, wife, and friend.", // User description to display in the author segment.
  copyright: "Copyright © 2020. All rights reserved.", // Copyright string for the footer of the website and RSS feed.
  themeColor: "#159484", // Used for setting manifest and progress theme colors.
  backgroundColor: "#159484" // Used for setting manifest background color.
};

// Validate

// Make sure pathPrefix is empty if not needed
if (config.pathPrefix === "/") {
  config.pathPrefix = "";
} else {
  // Make sure pathPrefix only contains the first forward slash
  config.pathPrefix = `/${config.pathPrefix.replace(/^\/|\/$/g, "")}`;
}

// Make sure siteUrl doesn't have an ending forward slash
if (config.siteUrl.substr(-1) === "/")
  config.siteUrl = config.siteUrl.slice(0, -1);

// Make sure siteRss has a starting forward slash
// if (config.siteRss && config.siteRss[0] !== "/")
//   config.siteRss = `/${config.siteRss}`;

module.exports = config;
