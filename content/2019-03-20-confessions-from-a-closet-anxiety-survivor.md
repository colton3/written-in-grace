---
date: 2019-03-20
title: 'Confessions from a "Closet" Anxiety Survivor'
cover: "./images/storm-new.jpg"
categories:
  - Anxiety
  - Depression
tags:
  - Christianity
  - Anxiety
  - Depression
  - Worry
  - God
  - COVID-19
---

## Am I alone?

Anxiety is a topic near and dear to my heart. It hasn’t always been that way, but a few years ago I
struggled with a major bout of anxiety unlike anything I had ever known before. It was the hardest
challenge I had ever faced due to the growing severity and length of time. During that struggle, I let no
one know that I was struggling. Unfortunately, that made for a very lonely battle. Throughout that entire
process, I began to feel crazy, literally crazy; everyone had their life together and no one else was
battling these overwhelming, breath-taking attacks of anxiety. So why was I?

But what I have since come to learn is that so many people have the same struggle and face anxiety to
varying degrees on a daily basis. Yet, none of us want to talk about our anxiety because it’s an
uncomfortable topic. And we feel crazy! Hence the name “closet” anxiety. But we need to talk about it.
And those facing it need to know they are not alone! I honestly feel like it is an issue plaguing not only
our nation, but unjustifiably our church.

However, I am here today to claim that God truly delivered me from my struggle with anxiety. Does that
mean I don’t have little flare ups and moments of intense worry? Absolutely not. But the person I am
today can smile at the peace God continues to pour out in my life in the face of scenarios that the old
me would have been paralyzed beneath the weight of.

<section><span>I am here today to claim that God truly delivered me from my struggle with anxiety</span></section>

Insert Covid-19. Talk about a wave of total unknowns coupled with mass hysteria on an epic level. Cue
the anxiety! Even if you are not one to normally struggle with anxiety, these recent events have been
enough to drive the thickest skin to show deep signs of worry. But I want to share my story and how God
intervened in my life to alleviate my anxiety in hopes that someone else, even if only one person, can
share in the same hope, relief, and peace that I have experienced.

## My Story

I will start by sharing a little backstory to my anxiety battle in the event that someone can relate (and to
show that the struggle was real! These aren’t just textbook theories dreamed up by someone in the
medical or psychological profession with no real firsthand experience. The anxiety was intense. And
frankly, often irrational! But welcome to anxiety…). Then I want to share exact steps of how God got me
through it and methods I continue to use today when facing the anxieties that still creep in.

I first started noticing my anxiety being set off by bad weather. I’ve had an unsettling fear of bad
weather since I was a kid, but after a certain point in time (when the tornadoes touched down in
Tuscaloosa, if you must know) it grew beyond unsettling to irrational. Every time the sky grew dark with
storm clouds, I would notice my heartrate begin to accelerate and my stomach would begin to feel
uneasy. Now would be an important time to mention I live in north Florida; if you don’t know, the sky
grows dark with intense thunderstorms nearly every afternoon for about 6 months of the year. The
storms don’t last long, and they are mostly harmless, but the damage they were doing to my mind was
crippling.

One afternoon I was pacing my office in fear, trying not to focus on the booming thunder and dark
clouds outside my window. I began talking with my coworker and asked her if she experienced any
anxieties, to which she responded that she had been on anxiety medication for years and highly
recommended it. Couldn’t live without it. So I went home that evening and talked with my husband
about the idea of medication. I had been telling myself that it was just thunderstorms causing the
anxiety, but if I was being honest, more and more topics (we’ll get there) were sparking those nervous thoughts, and my stomach was battling knots at an unchartable rate. My husband encouraged me and
said that if I thought medication was the route I needed to take he would support me. But I could not
shake this guilty feeling that medication was the “easy way out” and there were greater things in store
for me if I would choose the hard road of facing my anxiety in light of God’s word rather than running
from it. **LET ME STOP RIGHT HERE AND MAKE ONE THING VERY CLEAR: I AM NOT AGAINST
MEDICATION!** Everyone’s struggle is different and everyone’s path to recovery is different. I just know
that for me I did not feel like medication was the path I should choose. But my consideration of it was
enough to wake me up and realize I had a serious problem.

<section><span>I had been telling myself that it was just thunderstorms causing the
anxiety, but if I was being honest, more and more topics were sparking those nervous thoughts, and my stomach was battling knots at an unchartable rate</span></section>

So over the next few weeks I made an effort to be more conscious of what sparked my anxieties. And
boy did I come to find it was a lot more than thunderstorms. You name it, I sweated it. They started
slow. Money was a big one. My family’s health and wellbeing. Tornadoes. Christ’s return. Why I was
fearing Christ’s return (was I really saved?). What people would think of me knowing I questioned my
own salvation all while leading Bible studies and Sunday School classes. My house burning down if I left
the dryer running. Burglars getting in if I left the door unlocked (did I leave the door unlocked?!). Talks of
war. The sun burning out. Ya’ll…I told you things started getting irrational!

## The Snowball Effect

But that’s how anxiety works. It starts small. Just a worry or fear. But it goes left unchecked and then
you soon start to see the snowball. It’s almost like you get this overwhelming feeling that if you don’t
worry about it then who will? It’s on YOUR shoulders! You begin to worry about what it is that you are
not worrying about. Have you ever seen that meme that says something to the extent of “what your
mom is thinking when you don’t answer her texts” and then it shows pictures of a car crash and a
person drowning, indicating mom’s mind instantly jumps to the worst conclusion. We (or at least I)
nervously laugh at that meme because it hits far too close to home! I once drove to my mom’s house
(albeit 5 minutes away) because she wasn’t answering her texts and I cried in her front yard embracing
her in a hug as I found that her phone was unknowingly on silent and she was letting the dog out for a
potty break. Things were out of control. And that’s what I learned my anxiety boiled down to…I worried
over the things that were out of my control.

And I didn’t even mention the nightmares, which were truly probably the worst part of my battle. Let’s
face it, all that pent up anxiety during the day needs somewhere to release itself, and the relaxed,
sleeping mind is a playground for the anxiety to unwind.

Can you relate to any part of my story? Are you at the point of losing your mind in this daunting battle of
the brain running wild with your thoughts and perceived realities? You are not alone. And you are not
trapped there forever (praise the Lord)!

I heard someone say something that really affected my thinking: “A penny, when held close enough to
the eye, can blot out the sun.” And that’s exactly what was happening with my anxieties. I was allowing
these mostly miniscule fears to clog the forefront of my mind to the point that they were all I could see
or focus on. So I followed the advice a wise lady once told me, “do what you know to do until you know
what to do.” What I knew to do was pray, read God’s word, and trust that He would bring relief. Did it
happen overnight? No! In fact, it took several weeks and I was getting disheartened. But when I was at
my whit’s end, God showed up in mighty, personal ways that brought be comfort and ultimately peace.
Here are the lessons He taught me.

## How to Curb Anxiety:

1. **Take every thought captive**

   > “We destroy arguments and every lofty opinion raised against the knowledge of God, and take every thought captive to obey Christ.”
   >
   > **2 Corinthians 10:5**

   Analyze your thoughts. Sort through them to label them as harmful (reject!) or helpful (accept).

   - Does your thought begin with “What if?” Reject it! These are pretty much harmful 100% of the
     time and need to be rejected! We can “what if” ourselves to death. But the scenarios we dream
     up are not reality and are a dangerous, slippery slope leading to a spiraling destruction. “What
     if” thoughts are like potato chips…you can’t stop with just one.
   - Is this thought out of my control? Reject it! If there is nothing you can do to affect the outcome
     of the scenario, then worrying about it does nothing but harm. Trust that God is in control of the
     situation. (See point #3!)
   - Can I take action towards this thought? You were given a brain, use it! A lot of anxiety is caused
     by unknowns and unpreparedness. Think through the situation and make a plan. An example
     here would be your finances. Plan a budget and stick to it! Cut back on unnecessary purchases
     and be diligent in prioritizing need (electricity) vs want (cable) until the situation lightens.
     Remember, we have to think through the hard thoughts. If we don’t address them, they linger, grow, &amp;
     cloud up our minds. Our thoughts can become like the “monster in the closet” that children are so often
     afraid of. Once the parent inspects and shines light on the dark scary places revealing nothing but
     common closet items, the threat is diminished. Shine a light on what is clouding your mind and test each
     thought using the above criteria.

2. **Know what sets you off**

   > “Finally, brothers, whatever is true, whatever is honorable, whatever is just, whatever is pure, whatever is lovely, whatever is commendable, if there is any excellence, if there is anything worthy of praise, think about these things.”
   >
   > **Philippians 4:8**

   Each person’s anxiety is different, and each person’s trigger can be unique. But pay attention to what it
   is that starts your heart racing or draws your thoughts to a dark place. Then, by all means practical,
   avoid those set offs!

   - If watching the news stresses you, limit your sources and your amount of intake. Watching the
     news is not bad. But let’s be real, it’s often pretty bleak! Keeping it on 24/7 is not a wise idea if
     you experience anxiety surrounding world politics and events. Choose a news source or two that
     you trust, then set only a small amount of time (or number of articles) dedicated to its intake. I
     have often found that ignorance truly is bliss.
   - If scary movies or murder mystery shows/novels set you off, cut them out. You may enjoy them
     at the time, but if they are causing you anxiety later on then they are not worth it! Find
     something else that brings you positive and lasting pleasure.
   - If certain conversation topics (politics, war, current events) set you off, don’t feel obligated to
     continue in them. Excuse yourself from the conversation or simply state that you are
     uncomfortable discussing and would like to change the subject.

3. **Study the God you profess to trust in**

   > “Let us hold fast the confession of our hope without wavering, for He who promised is faithful.”
   >
   > **Hebrews 10:23**

   Let me say it louder for those in the back. If you are a Christian who professes to believe in the Almighty
   God, then why is anxiety controlling your life? This was the breaking point in my anxiety!

   - Be reassured that if God really is who His Word says He is (and He is!!), then He is fully capable
     to take care of any situation you may be facing.
   - Are your thoughts/actions bringing glory to God? Is your mind at war with what you claim to
     believe versus what you are letting control your thoughts? Feelings of hypocrisy can heighten
     anxiety. Study God’s word and nail down what you believe in Him to bring your mind peace. I
     promise you won’t regret it!
   - Study and memorize scripture related to your specific worries. Then when those fears arise,
     quote scripture and shut down that fear! (Matthew 6:25-34 is a great starting point!)
     Really, think about it. We as Christians believe in the omnipotent, omniscient, omnipresent (aka all-
     powerful, all-knowing, all-present) God who loves us each enough to have a personal relationship with
     us through his Son Jesus whom He sent to die and raise again in order that we might have that personal
     connection. And you think He can’t handle your debt problems?? You think He can’t stop a Coronavirus?
     You think He’s oblivious to your struggles? Have faith that God is in control despite whatever negative
     circumstances are currently prevalent in your life.

4. **Have you prayed about it as much as you have worried about it?**

   > “Do not be anxious about anything, but in everything by prayer and petition present your requests to the Lord. And the peace of God which transcends all understanding will guard your hearts and minds in Christ Jesus.”
   >
   > **Philippians 4:6-7**

   I’m just going to leave this one right here; I think it speaks for itself. And if your answer is no, you’re not
   alone! I struggle here too. But don’t rule prayer out! God wants to hear from you.

5. **Talk your thoughts out loud with someone you trust and can confide in**

   > “Which of you by being anxious can add a single hour to his life span?”
   >
   > **Matthew 6:27**

   Thoughts are one thing, but when you have to say them out loud they can take on a whole new form.

   - Sometimes you don’t realize the level of irrationality until you have to vocalize it. (Yikes! Scratch
     that fear…I just heard how silly it sounds out loud.)
   - Let your confidant speak words of truth over you. Be willing to listen as they help you talk out
     your anxieties.
   - If possible, compare your fears to historical events. This may sound silly, but hear me out! This
     can be either your own history or the world’s depending on the circumstance. If it’s finances,
     remind yourself of a time when God provided when you were in a tight spot. If it’s Covid-19,
     think about other crazy spreading diseases like the black plague that yes were bad, but the
     world got through it. I find this approach surprisingly helpful and comforting.

So there you have it. It may not be rocket science, but these steps changed my life drastically! For
example, I have grown to love thunderstorms! Who knew?! The girl who paced the floor when a dark
cloud passed by now loves to sit and watch the sky turn dark and watch the lightning streak the sky. And
as silly as it may seem, I smile each time a storm is rolling in because I remember those old feelings as a
distant memory and I thank God that He delivered me from them. Covid-19 would have had the old me
paralyzed in fear, but instead I am choosing to embrace God and His word and trust that He is in control
and will receive glory through this all. I hope that you too will be able to “experience the peace of God
that transcends all understanding.”

> “Peace I leave with you; my peace I give to you. Not as the world gives do I give to you. Let not your hearts be troubled, neither let them be afraid.”
>
> **John 14:27**
