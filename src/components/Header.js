import React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import styles from "./Header.module.scss";

const Header = () => {
  const data = useStaticQuery(graphql`
    query GetHeaderImage {
      file(relativePath: { eq: "images/grace.png" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  const image = data.file.childImageSharp;

  return (
    <header>
      <Link to="/" activeClassName={styles.activeNav}>
        <Img fluid={image.fluid} style={{ height: `100%`, width: `300px` }} />
      </Link>
    </header>
  );
};

export default Header;
