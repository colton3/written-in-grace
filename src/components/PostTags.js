import React from "react";
import styles from "./PostTags.module.scss";

const PostTags = ({ tags }) => {
  return (
    <div className={styles.tagContainer}>
      {tags && tags.map((tag, idx) => <span key={idx}>{tag}</span>)}
    </div>
  );
};

export default PostTags;
