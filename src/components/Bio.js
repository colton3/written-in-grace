import React from "react";

const Bio = ({ config, expanded }) => (
  <>
    <p>
      Written by <strong>{config.userName}</strong>
    </p>
  </>
);

export default Bio;
