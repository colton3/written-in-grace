import React from "react";
import Helmet from "react-helmet";
import { graphql, useStaticQuery } from "gatsby";
import Layout from "../layout";
import Bio from "../components/Bio";
import PostTags from "../components/PostTags";
import SocialLinks from "../components/SocialLinks";
import SEO from "../components/SEO";
import config from "../../data/SiteConfig";
import styles from "../templates/post.module.scss";
import "../templates/prism-okaidia.css";

export default ({ pageContext }) => {
  const data = useStaticQuery(graphql`
    query BlogPost {
      markdownRemark(
        fields: { slug: { eq: "/confessions-from-a-closet-anxiety-survivor" } }
      ) {
        html
        timeToRead
        excerpt
        frontmatter {
          title
          cover {
            publicURL
            childImageSharp {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          date
          categories
          tags
        }
        fields {
          slug
          date(formatString: "MMMM DD, YYYY")
        }
      }
    }
  `);

  const postNode = data.markdownRemark;
  const post = postNode.frontmatter;
  const date = postNode.fields.date;

  return (
    <Layout>
      <main>
        <Helmet>
          <title>{`${post.title} | ${config.siteTitle}`}</title>
        </Helmet>
        <SEO postPath={"/"} postNode={postNode} postSEO />
        <div>
          <h1>{post.title}</h1>
          <img
            src="https://images.unsplash.com/photo-1493243350443-9e3048ce7288?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2552&q=80"
            alt="storm"
            style={{
              maxHeight: `350px`,
              width: `100%`,
              objectFit: `cover`,
              objectPosition: `center center`
            }}
          />
          <p className={styles.postMeta}>
            {date} &mdash; {postNode.timeToRead} Min Read{" "}
          </p>
          <div className={styles.postMeta}>
            <PostTags tags={post.tags} />
          </div>
          <div dangerouslySetInnerHTML={{ __html: postNode.html }} />

          <hr />
          <Bio config={config} />
          <div className={styles.postMeta}>
            <SocialLinks postPath={"/"} postNode={postNode} />
          </div>
        </div>
      </main>
    </Layout>
  );
};
